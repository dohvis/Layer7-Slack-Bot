# -*- coding: utf-8 -*-
# for debug

from slack import *
import atexit

@atexit.register
def goodbye():
    s = Slack()
    token = s.token
    channel = s.channel
    sc = SlackClient(token)
    print "You are now leaving the Python sector."                
    sc.rtm_connect()
    sc.rtm_send_message(channel,"-"*40+"\n[-]Layer7's BOT is killed!\n"+"-"*40)  

if __name__ == "__main__":
    s = Slack()
    sc = SlackClient(s.token)
    c = s.channel
    sc.rtm_connect()
    msg = "<http://naver.com|naver.com>"
    sc.rtm_send_message(c,msg)
    s.receiver()
    
