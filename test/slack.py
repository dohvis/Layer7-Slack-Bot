# -*- coding: utf-8 -*-
#Debug
from slackclient import SlackClient
import datetime
from sympy import sympify 
from json import loads,dumps                      
from tool import Tools
from log import *
from time import sleep
from re import search
class Slack(Tools,Log):
    def __init__(self):
        from ConfigParser import RawConfigParser
        cfg = RawConfigParser()
        cfg.read("./setting.ini") 
        self.token =  cfg.get('setting','token')# layer7_bot
        self.sc = SlackClient(self.token)
        self.members = {}
        self.channel =  cfg.get('setting','channel')

    def get_user_name(self,key):
        try:
            name = self.members[key]
        except KeyError:
            #print key
            user_info = self.sc.api_call("users.info",token=self.token,user=key)
            user_info = loads(user_info)
            name = user_info['user']['name']
            self.members['key']=name
        return name

    def receiver(self):
        if self.sc.rtm_connect():
            print "[+] Successful Connection !"
            self.sc.rtm_send_message(self.channel,"-"*40+"\nLayer7's BOT is Online!\n"+"-"*40)

            while True:
                sleep(0.5)
                data = self.sc.rtm_read()
                l = len(data)
                if l == 1: 
                    data = data[0]
                    self.type_handler(data)
        else:
            print "ERROR on rtm_connect! Plz Confirm token"

    def type_handler(self,data):
        _type = data.get('type')
        print _type
        if _type == "message":
            print data

            try:
                user = self.get_user_name(data.get('user'))

            except KeyError:
                print "unknown user "
                try:
                    if data.get('subtype') == "bot_message":
                        return;
                except:
                    self.sc.rtm_send_message(self.channel,"who are you?")

            except UnboundLocalError:
                print "unknown user "
                self.sc.rtm_send_message(self.channel,"who are "+data['user'])
            try:
                text = data["text"]
                try:
                    self.message_handler(user,text)
                except UnicodeError:
                    print "Unicode is not supported.."
 
            except KeyError:
                print "type is message but text is not str"
        
        elif _type == "user_typing":
            pass

        elif _type == "presence_change":
            user = self.get_user_name(data.get('user'))
            if data["presence"] == "active":
                self.sc.rtm_send_message(self.channel,u"[%s] ㅎㅇ " % user)
            else:
                self.sc.rtm_send_message(self.channel,u"[%s] 빠빠이" % user)
            self.user_logging(user,data["presence"])
        
        else:
            print data

    def message_handler(self,user,text):
        if u"\ubc30\uace0" in text:
            self.sc.rtm_send_message(self.channel,"Are You Hungry ? :) ")
            message = self.get_food_img("%EC%9C%84%EA%BC%B4")
            self.sc.rtm_send_message(self.channel,message)

        if text[0] != '!':
            pass
        elif user == "marcia":
            self.sc.rtm_send_message(self.channel,u"ㅗㅗ")
        else:
            text = text.split()        
            if text[0] == "!who":
                msg = ""
                for key in self.get_current_online():
                    msg += self.get_user_name(key)+" , "
                self.sc.rtm_send_message(self.channel,msg)
            elif text[0] == "!hello":
                message = "hello %s !" % user
                self.sc.rtm_send_message(self.channel,message)

            elif text[0] == "!hungry":
                if len(text) == 1:
                    message = self.get_food_img("%EC%9C%84%EA%BC%B4") # dnlRhf
                    
                else:
                    food = text[1]
                    message = self.get_food_img(food)

                self.sc.rtm_send_message(self.channel,message)

            elif text[0] == "!date":
                day = datetime.date.today()
                dateee = "today is " + str(day.year) + "-" + str(day.month) + "-" + str(day.day)
                self.sc.rtm_send_message(self.channel, dateee)

            elif text[0] == "!echo":
                pass
            elif text[0] == "!help":
                msg = """```
Layer7 Slack Bot\n
!hello 하고 말을 걸어보세요\n
##
소괄호로 표시된 인자는 기본값이 미리 정해져 있습니다 ex)!hungry\n
!date  날짜를 표시해줍니다!\n
!base64encode [encode할 문자]\n
!base64decode [decode할 문자]\n
!hungry (먹고싶은 음식)  배고픈 그들을 위해 위꼴짤을 보내드립니다\n
!calc [계산할 수식] 계산기도 되여!
!whoami 누구세여
!last (닉네임) (표시할 행 수)
!md5 [encode할 문자]\n
##
```"""
                self.sc.rtm_send_message(self.channel, msg)
                
            elif text[0] == "!calc":   
                if "+" in str(text) or "-" in str(text) or "*" in str(text) or "/" in str(text) or "**" in str(text) or "^" in str(text) or "%" in str(text):
                    calc = text[1:]
                    if "**" in str(calc):
                        if 100 < int(search(r"(?<=\*\*)(\d*)",str(calc)).group(),10):
                            # ** 로 뻗는거 방지 
                            self.sc.rtm_send_message(self.channel,r"\*\*(\d*) < 100")
                            return;
                    print (calc)
                    try:
                        res =  sympify(calc)
                    except:
                        self.sc.rtm_send_message(self.channel, u"ㅗ")
                    try:
                        self.sc.rtm_send_message(self.channel, str(res))
                    except:
                        pass

            elif text[0] == "!last":
                if len(text) == 1:
                    rows = self.get_user_log(user,1)
                elif len(text) == 2:
                    rows = self.get_user_log(text[1],1)
                elif len(text) == 3:
                    rows = self.get_user_log(text[1],text[2])
                else:
                    pass
                try:
                     msg = "```user : %s\n" % rows[0][0]
                except IndexError:
                    self.sc.rtm_send_message(self.channel,"does not have log for you")
                    return;
                except:
                    print "Invalid argument"
                    return; 
                for i in range(0,len(rows)):
                    msg += "join time : %s\n" % rows[i][1]
                    msg += "leave time : %s\n" % rows[i][2]
                    try:
                        s = int(rows[i][3]) % 60
                        m = int(rows[i][3])//60
                    except TypeError:
                    # 가끔 row가 None 리턴할때 예외처리
                        s = 0
                        m = 0
                    if m > 60:
                        h = m // 60
                        msg += "total : %sh %dm %ds\n" % (h,m,s)
                    else:
                        msg += "total : %sm %ss\n" % (m,s)
                self.sc.rtm_send_message(self.channel,msg+"```")
                
            elif text[0] == "!whoami":
                rows = self.get_user_log(user,0)
                if len(rows) == 0:
                    self.sc.rtm_send_message(self.channel,"[-] does not have log for you")
                    return;
                last_join = rows[0][1]
                
                period = datetime.datetime.now()-last_join 
                msg = "```Your name : %s\n" % user
                msg += "Your join time : %s\n" % last_join
                msg += "Connecting %s \n" % str(period)[:-7]
                self.sc.rtm_send_message(self.channel,msg+"```")
           
            elif text[0][:4] == "!md5":
                res = self.md5(text[1])
                self.sc.rtm_send_message(self.channel,res)

            elif text[0][:7] == "!base64":
                msg = self.base64(text)
                self.sc.rtm_send_message(self.channel, msg)

            elif text[0] == "!naver":
                baa = self.naver_realtime_srch()
                self.sc.rtm_send_message(self.channel, baa)
            else:
                self.sc.rtm_send_message(self.channel, "Invalid Command")
