#!/usr/bin/env python
# -*- coding: utf-8 -*-
from slackclient import SlackClient
import datetime
from sympy import sympify 
from json import loads,dumps                      
from tool import Tools
from log import *
from time import sleep
from re import search
class Slack(Tools, Log):
    def __init__(self):
        from settings import API_TOKEN, CHANNEL
        self.token =  API_TOKEN
        self.sc = SlackClient(self.token)
        self.members = {}
        self.channel = CHANNEL
        
    def get_user_name(self,key):
        try:
            name = self.members[key]
        except KeyError:
            #print key
            user_info = self.sc.api_call("users.info",token=self.token,user=key)
            user_info = loads(user_info)
            name = user_info['user']['name']
            self.members['key']=name
        return name

    def receiver(self):
        if self.sc.rtm_connect():
            print "[+] Successful Connection !"
            self.sc.rtm_send_message(self.channel,"-"*40+"\nLayer7's BOT is Online\n"+"-"*40)

            while True:
                sleep(0.1)
                data = self.sc.rtm_read()
                l = len(data)
                if l == 1: 
                    data = data[0]
                    self.type_handler(data)
        else:
            print "ERROR on rtm_connect! Plz Confirm token"

    def type_handler(self,data):
        _type = data.get('type')
        print _type
        print data
        if data.get('channel') != self.channel and data.get('channel')!=None:
            return;
        if _type == "message":
            print data

            try:
                user = self.get_user_name(data.get('user'))

            except KeyError:
                print "unknown user "
                try:
                    if data.get('subtype') == "bot_message":
                        return;
                except:
                    self.sc.rtm_send_message(self.channel,"who are you?")

            except UnboundLocalError:
                print "unknown user "
                self.sc.rtm_send_message(self.channel,"who are "+data['user'])
            try:
                text = data["text"]
                try:
                    msg = self.message_handler(user,text)
                    self.sc.rtm_send_message(self.channel, msg)
                except UnicodeError:
                    print "Unicode is not supported.."
            except KeyError:
                print "type is message but text is not str"
            
        elif _type == "user_typing":
            pass

        elif _type == "presence_change":
            user = self.get_user_name(data.get('user'))
            print data
            if data["presence"] == "active":
                self.sc.rtm_send_message(self.channel,u"[%s] hello " % user)
                pass
            else:
                self.sc.rtm_send_message(self.channel,u"[%s] bye " % user)
            self.user_logging(user,data["presence"])
        
        else:
            print data

    def message_handler(self,user,text):
        if u"\ubc30\uace0" in text:
            return "Are You Hungry ? :) "
            message = self.get_food_img("%EC%9C%84%EA%BC%B4")
            return message

        if text[0] != '!':
            pass
        elif user == "marcia":
            return u"ㅗㅗ"
        else:
            text = text.split()        
            if text[0] == "!hello":
                message = "hello %s !" % user
                return message
            elif text[0] == "!SO" and len(text) == 2:
                msg = self.get_stackoverflow(text[1])
                return msg
            elif text[0] == "!ping":
                if len(text) != 2:
                    print "invalid used"
                    return;
                ip_regex = r"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$"
                domain_regex = r"[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\.[a-zA-Z]{2,3})"
                if bool(search(ip_regex,text[1])) == True:
                    res = self.ping(text[1])
                    return res
                elif bool(search(domain_regex,text[1])) == True:
                    
                    host = search(domain_regex,text[1]).group()
                    res = self.ping(host)
                    return res
                else:
                    print "ping fail"
                    return "Invalid HOST"
            elif text[0] == "!hungry":
                if len(text) == 1:
                    message = self.get_food_img("%EC%9C%84%EA%BC%B4") # dnlRhf
                    
                else:
                    food = text[1]
                    message = self.get_food_img(food)

                return message

            elif text[0] == "!date":
                day = datetime.date.today()
                dateee = "today is " + str(day.year) + "-" + str(day.month) + "-" + str(day.day)
                return dateee

            elif text[0] == "!echo":
                pass
            elif text[0] == "!help":
                msg = """```
                Layer7 Slack Bot\n
                !hello 하고 말을 걸어보세요\n
                ##
                소괄호로 표시된 인자는 기본값이 미리 정해져 있습니다 ex)!hungry\n
                !date  날짜를 표시해줍니다!\n
                !base64encode [encode할 문자]\n
                !base64decode [decode할 문자]\n
                !hungry (먹고싶은 음식)  배고픈 그들을 위해 위꼴짤을 보내드립니다\n
                !calc [계산할 수식] 계산기도 되여!
                !whoami 누구세여
                !last (닉네임) (표시할 행 수)
                !md5 [encode할 문자]\n
                ##
                ```"""
                return msg
                
            elif text[0] == "!calc":   
                if "+" in str(text) or "-" in str(text) or "*" in str(text) or "/" in str(text) or "**" in str(text) or "^" in str(text) or "%" in str(text):
                    calc = text[1:]
                    if "**" in str(calc):
                        if 100 < int(search(r"(?<=\*\*)(\d*)",str(calc)).group(),10):
                            # ** 로 뻗는거 방지 
                            return r"\*\*(\d*) < 100"
                            return;
                    print (calc)
                    try:
                        res =  sympify(calc)
                    except:
                        return u"ㅗ"
                    try:
                        return str(res)
                    except:
                        pass

            elif text[0] == "!last":
                if len(text) == 1:
                    data = self.user_last()
                elif len(text) == 2:
                    if int(text[1]) > 100:
                        return " maximum == 100"
                        return;
                    data = self.user_last(text[1])
                else:
                    return "```"+"Invalid Option" + "```"
                    return;
                msg = "%-10s %-10s\n"%("NAME", "LEAVE TIME")
                if data is None:
                    return;
                for i in data:
                    msg += "%-10s %-10s\n" %(i[0], i[1])
                
                return "```"+msg + "```"

            elif text[0] == "!last_":
                if len(text) == 1:
                    rows = self.get_user_log(user,1)
                elif len(text) == 2:
                    rows = self.get_user_log(text[1],1)
                elif len(text) == 3:
                    rows = self.get_user_log(text[1],text[2])
                else:
                    pass
                try:
                     msg = "```user : %s\n" % rows[0][0]
                except IndexError:
                    return "does not have log for you"
                    return;
                except:
                    print "Invalid argument"
                    return; 
                for i in range(0,len(rows)):
                    msg += "join time : %s\n" % rows[i][1]
                    msg += "leave time : %s\n" % rows[i][2]
                    try:
                        s = int(rows[i][3]) % 60
                        m = int(rows[i][3])//60
                    except TypeError:
                    # 가끔 row가 None 리턴할때 예외처리
                        s = 0
                        m = 0
                    if m > 60:
                        h = m // 60
                        msg += "total : %sh %dm %ds\n" % (h,m,s)
                    else:
                        msg += "total : %sm %ss\n" % (m,s)
                return msg+"```"
                
            elif text[0] == "!whoami":
                rows = self.get_user_log(user,0)
                if len(rows) == 0:
                    return "[-] does not have log for you"
                
                last_join = rows[0][1]
                
                period = datetime.datetime.now()-last_join 
                msg = "```Your name : %s\n" % user
                msg += "Your join time : %s\n" % last_join
                msg += "Connecting %s \n" % str(period)[:-7]
                return msg+"```"
           
            elif text[0][:4] == "!md5":
                res = self.md5(text[1])
                return res

            elif text[0][:7] == "!base64":
                msg = self.base64(text)
                return msg

            elif text[0] == "!naver":
                res = self.naver_realtime_srch()
                return res
            elif text[0] == "!simsim":
                from random import choice
                func = choice(['get_beobe()', 'get_github_trending()'])
                return eval("self."+func)
                
            else:
                return "Invalid Command"
