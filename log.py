# -*- coding: utf-8 -*-
import MySQLdb
import datetime

class Log:
    def user_last(self,*limit):
        from settings import DB_USER, DB_PASSWORD, DB_NAME
        self.user = DB_USER
        self.pw = DB_PASSWORD
        self.db = DB_NAME
        db = MySQLdb.connect(host="localhost", user=self.user , passwd=self.pw, db = self.db)
        cur = db.cursor()
        if len(limit) != 0:
            try:
                limit = int(str(limit[0]))
            except:
	        print 'error'
		return
	    limit = str(limit)
            q = "select name, leave_time from log order by leave_time desc limit "+limit
        else:
            q = "select name, leave_time from log order by leave_time desc limit 5"
        cur.execute(q)
        res = cur.fetchall()
        print res
        return res

    def user_logging(self,user,presence):

        """
        테이블 구조 
        CREATE DATABASE layer7_bot;
        use layer7_bot
        CREATE TABLE users(name text not null);
        CREATE TABLE log(name text,join_time timestamp, leave_time timestamp, period int); # period is sector
        INSERT INTO users values('ner0'),('tmdgus'),('roxer'),('who1sth1s'),('ghdhd21'),('r3tina'),('aaazzz135');
        

        """
        from settings import DB_USER, DB_PASSWORD, DB_NAME
        self.user = DB_USER
        self.pw = DB_PASSWORD
        self.db = DB_NAME
        db = MySQLdb.connect(host="localhost", user=self.user , passwd=self.pw, db = self.db)
            
        cur = db.cursor()
        if presence == "active":
            q = """ UPDATE log
            SET
                leave_time = NOW()-1
                WHERE name='%s' and period is NULL
            """ % user
           
            cur.execute(q)
            q = "SELECT join_time,leave_time FROM log WHERE name='"+user+"'"
            cur.execute(q)
            for row in cur.fetchall():
                if row[1] is None:
                    # NULL값 방지, period 가 NULL 일경우 이렇게 하드코딩해서 넣어줌 
                    cur.execute("UPDATE log SET leave_time = NOW()-1,period=0 WHERE join_time='"+str(row[0])+"'")

            q = """ INSERT INTO log(`name`,`join_time`) VALUES('%s',NOW()) """ % user

        else:
            # 유저가 접속종료할때
            cur.execute("SELECT join_time FROM log WHERE name='%s' order by join_time desc limit 1" % user)
            lastest = []
            for row in cur.fetchall() :
                lastest = row[0]
                print lastest

            if len(str(lastest))==0:
                return;

            q = """ UPDATE log 
            SET leave_time=NOW(),
             period=TIMESTAMPDIFF(SECOND,'%s',NOW())
            WHERE join_time='%s'
               """ % (lastest,lastest)

            
        try:
            cur.execute(q)
            db.commit()
            print "Logging Success "+q
        except:
            print "Query Fail"
        db.close()

    def get_user_log(self,user,limit):
        db = MySQLdb.connect(host="localhost",
                     user="root", 
                      passwd="stamp",
                      db="layer7_bot")
        limit = str(limit)
        cur = db.cursor()
        q = """ SELECT name,join_time,leave_time,period FROM log WHERE name='%s' order by join_time desc limit 1,%s""" % (user,limit)
        if limit=="0":
            q = """ SELECT name,join_time,leave_time,period FROM log WHERE name='%s' order by join_time desc limit 1""" % user
            
        try:
            cur.execute(q)
        except:
            print "Query Error"
            return;
        rows = []
        rows = [row for row in cur.fetchall()]
        print rows
        return rows
