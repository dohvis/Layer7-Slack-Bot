# -*- coding: utf-8 -*-
from collections import Counter
from konlpy.tag import Hannanum
from urllib2 import urlopen,Request
from bs4 import *
from xml.etree.ElementTree import parse
from re import compile,search
import sys
from os.path import isfile

reload(sys)
sys.setdefaultencoding('utf-8')

def get_text(url):
    html = urlopen(url).read()
    s = BeautifulSoup(html)
    print len(html)
    ss= ""
    for x in s.find('body').stripped_strings:
        try:
            ss += x.encode('utf-8')+"\n"
        except:
            print type(x)
    return ss

def get_naver_text(url):
    html = urlopen(url).read()
    s = BeautifulSoup(html)
    res = ''
    print len(html)
    text_lists = s.find_all('p',{"class":"txt"})
    for p in text_lists:
        for word in p.stripped_strings:
            try:
                res += word.encode('utf-8')+"\n"
            except:
                print type(word)
    return res

def get_tags(text, ntags=50, multiplier=10):
    h = Hannanum()
    nouns = h.nouns(text)
    count = Counter(nouns).most_common(50)
    return (nouns,count)
    
def google_search_summary(keyword,engine="google"):
    url = "https://www.google.com/search?q=%s&newwindow=1&tbs=lr:lang_1ko&lr=lang_ko" % keyword

    headers = {'User-Agent':'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'}
    req = Request(url,None,headers)
    res = urlopen(req).read()
    
    s = BeautifulSoup(res)
    #print s
    print s.find('h3',{"class":"r"})
    urls = [(u.a['href']) for u in s.find_all('h3',{'class':'r'})]
    print urls

def naver_search_summary(keyword,target="encyc"):
    key = "899811204670c2bc188e3d81c9fc901c"
    url = "http://openapi.naver.com/search?key="+key+"&query="+keyword+"&target="+target+"&start=1&display=10"
    request = Request(url, headers={"Accept" : "application/xml"})
    u = urlopen(request)
    soup = BeautifulSoup(u.read())
    print soup.title.string
    links = []
    desc = []
    for e in soup.find_all("item"):
        stre = str(e)
        from re import search
        l = search(r'http://(.*?)(?=<)',stre).group()
        links.append(l)
        desc.append(e.description.string.encode('utf-8'))
    return (links,desc)

def is_hangul(text):
    """
    hangul = compile('[^ \u3131-\u3163\uac00-\ud7a3]+')
    hangul.findall(text)
    """
    hangul = compile('[ ㄱ-ㅣ가-힣]+')
    return bool(hangul.findall(text))

def md5(text):
    from hashlib import md5
    m = md5()
    m.update(text[:128])
    return m.hexdigest()

def save_file(text):
    f = open(md5(text),'w')
    f.write(text)
    f.close()


if __name__ == "__main__":
    links,desc = naver_search_summary(sys.argv[1])
    print links
    text = ""
    for i in links:
        tmp = get_naver_text(i)
        #save_file(tmp)
        text += tmp

    n,t = get_tags(text)
    for n,c in t:
        if is_hangul(n.encode()) == True:
            print n.encode()+" : "+str(c)
            

