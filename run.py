#!/usr/bin/env python
# -*- coding: utf-8 -*-

from slack import *
import atexit

@atexit.register
def goodbye():
    s = Slack()
    token = s.token
    channel = s.channel
    sc = SlackClient(token)
    print "You are now leaving the Python sector."                
    sc.rtm_connect()
    sc.rtm_send_message(channel,"-"*40+"\n[-]Layer7's BOT is killed!\n"+"-"*40)  

if __name__ == "__main__":
    s = Slack()
    s.receiver()
    
