# -*- coding: utf-8 -*-
from bs4 import BeautifulSoup
from urllib2 import *
from urllib import quote_plus
from random import randint
from slack import *
from requests import get

class GetOutOfLoop( Exception ):
    pass

class Tools():
    def ping(self,ip):
        import subprocess as s
        fd = s.Popen("ping -c 5 "+ip,shell=True,
            stdin=s.PIPE,
            stdout=s.PIPE,
            stderr=s.PIPE)
        
        return fd.stdout.read()

    def get_opgg(self,nick):
      url = ""

    def get_food_img(self,food):
        url = "https://www.google.co.kr/search?q=%s" % food
        url += "&newwindow=1&source=lnms&tbm=isch&sa=X&ei=_GfrVMmLOtK58gWl9YKgAw&ved=0CAgQ_AUoAQ&biw=1366&bih=653#q="+food+"&newwindow=1&tbm=isch&tbs=isz:l"
        print url
        req = Request(url,headers={'User-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.0 Safari/537.36'})
        html = urlopen(req).read()
        f = open("food.html","wb")
        f.write(html)
        b_html = BeautifulSoup(html, "html.parser")
        a_list = b_html.find_all('a',{"class":"rg_l"})
        result = []
        for a in a_list:
            from re import findall
            result.append(re.findall(r"imgurl=(.*?)&",a['href'])[0])
        print result  
        try:
            return result[randint(0,len(result))]
        except IndexError:
            return False

    def md5(self,string):
        from hashlib import md5
        m = md5()
        m.update(string)
        return m.hexdigest()
        
    def naver_realtime_srch(self):
        url = "http://www.naver.com"
        try:
            html = urlopen(url).read()
        except HTTPError as e:
            print(e)
            print("[-] Retry..")
            from time import sleep
            sleep(2)
            try:
                html = urlopen(url).read()
            except:
                print('Fuck')
                return False
        rt_list = [] # 실검 리스트 
        rt_str="""```실시간 인기 검색어\n"""
        soup = BeautifulSoup(html, "html.parser")
        realtime_tag = str( soup('noscript') )
        realtime = realtime_tag.split('<option value=')
        for r in realtime:
            r.decode('utf-8')
            index = r.index('<')
            rt_list.append(r[:index])
        base_url = " http://search.naver.com/search.naver?query="
        for r in rt_list:
            if rt_list.index(r) != 0:
                rt_str += str(rt_list.index(r))+" "
                rt_str += r + base_url+quote_plus(r.replace('"',''))+"\n"
                
        rt_str+="```"
        return rt_str

    def get_stackoverflow(self,keyword):
        url ="http://stackoverflow.com/questions/tagged/%s?sort=votes&pageSize=30" % keyword
        html = urlopen(url).read()
        soup = BeautifulSoup(html, "html.parser")
        titles = soup('a',{"class":"question-hyperlink"})
        rt_str = ""

        for i in titles:
            rt_str += i.string+" "
            rt_str += "http://stackoverflow.com"+i['href']+"\n"
        return rt_str

    def base64(self):
        from base64 import encodestring,decodestring

        string = text[1]
        if text[0][7:] == "encode":
            try:
                msg = encodestring(string)
            except:
                msg = "Fail to Encode"
        elif text[0][7:] == "decode":
            try:
                msg = decodestring(string)
            except:
                msg = "fail to Decode"
        else:
            msg = "!base64encode string\n!base64decode encodestring";

        return msg

    def get_beobe(self):
        from urllib import unquote
        html = get('http://beobe.us/').content
        soup = BeautifulSoup(html, "html.parser")
        hot = soup.find('div', {'class': 'list-group'})
        
        res = ""
        for link in list(hot.find_all('a'))[:10]:
            title = list(link.stripped_strings)[0]
            try:
                for word in [u"17", u"18", u"19", u"처자", u"몸", u"볼륨", u"관계", u"모텔"]:
                    if word in title:
                        print '19@'
                        raise GetOutOfLoop
            except GetOutOfLoop:
                continue
                    
            res += "%s\t%s\n" % \
            (title, unquote(link['href'].split("href=")[-1]))

        return res
   
    def get_github_trending(self, lang="python"):
        soup = BeautifulSoup(get("https://github.com/trending?l="+lang).content)
        h3s = soup.find_all('h3', {'class': 'repo-list-name'})
        res = ""
        for h3 in h3s:
            res += "%s\t%s%s\n" % \
            (list(h3.stripped_strings)[-1],
            "https://github.com/", h3.contents[1]['href'])

        return res

